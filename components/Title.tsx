import React from 'react';
import './title.scss';

export default function Title(props) {
  const { title, description } = props;

  return (
    <div className="title">
      <h3 className="title-name">{title}</h3>
      <p>{description}</p>
    </div>
  );
}
