import './customButton.scss';

export default function CustomButton(props) {
  const { title, className } = props;
  return <button className={className}>{title}</button>;
}
