import React from 'react';
import './recentUpdates.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Title from '../Title';
import CustomButton from '../CustomButton';
import Posts from '@/components/Posts/page';

export default function RecentUpdates() {
  return (
    <Container>
      <Title title="Recent Updates" description={null} />
      <Row className="mb-5 ">
        <Col lg={4} className="p-3 mb-4">
          <Posts />
        </Col>
        <Col lg={4} className="p-3 mb-4">
          <Posts />
        </Col>
        <Col lg={4} className="p-3 mb-4">
          <Posts />
        </Col>
        <Col lg={4} className="p-3 mb-4">
          <Posts />
        </Col>
        <Col lg={4} className="p-3 mb-4">
          <Posts />
        </Col>
      </Row>
      <div className="button-RecentUpdates">
        <CustomButton title="View more" className="btn-viewMore" />
      </div>
    </Container>
  );
}
