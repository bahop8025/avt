import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Title from '../Title';
import './ourMission.scss';
import Link from 'next/link';
import Image from 'next/image';
import image from '../../public/img1.jpg';
import imageSto from '../../public/img_story.png';

export const OurMission = () => {
  return (
    <Container>
      <Title
        title="Our Mission"
        description="Our mission is to better the lives of consumers and small businesses with technology."
      />
      <div className="ourMission_content">
        <Row>
          <Col lg={4} className="p-3">
            <Link href={'#'} className="ourMission">
              <div className="ourMission-image">
                <Image alt="image " src={image} />
              </div>
              <div className="ourMission-text">
                <Image src={imageSto} alt="image" />
                <h6>About Us</h6>
                <p>Learn more about who we are</p>
              </div>
            </Link>
          </Col>
          <Col lg={4} className="p-3">
            <Link href={'#'} className="ourMission">
              <div className="ourMission-image">
                <Image alt="image " src={image} />
              </div>
              <div className="ourMission-text">
                <Image src={imageSto} alt="image" />
                <h6>About Us</h6>
                <p>Learn more about who we are</p>
              </div>
            </Link>
          </Col>
          <Col lg={4} className="p-3">
            <Link href={'#'} className="ourMission">
              <div className="ourMission-image">
                <Image alt="image " src={image} />
              </div>
              <div className="ourMission-text">
                <Image src={imageSto} alt="image" />
                <h6>About Us</h6>
                <p>Learn more about who we are</p>
              </div>
            </Link>
          </Col>
        </Row>
      </div>
    </Container>
  );
};
