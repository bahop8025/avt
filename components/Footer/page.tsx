import React from 'react';
import './footer.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';
import imageFooter from '../../public/Logo_big.png';

const FooterBotom = () => {
  return (
    <div className="footer-botom">
      <span>
        Copyright © Sea Limited. Trademarks belong to their respective owners. All
        rights reserved.
      </span>
      <span className="line">|</span>
      <Link href={'#'}>Terms of Service</Link>
      <span className="line">|</span>
      <Link href={'#'}>Privacy Policy</Link>
    </div>
  );
};

const FooterTop = () => {
  return (
    <div className="footer-top">
      <Row>
        <Col lg={5}>
          <Row>
            <Col lg={12}>
              <div>
                <Link href={'#'}>
                  <Image src={imageFooter} alt="image" className="image-Footer" />
                </Link>
                <p>1 Fusionopolis Place, #17-10, Galaxis, Singapore 138522</p>
                <Link href="Tel:+65 6270 8100">
                  <p>Tel:+65 6270 8100</p>
                </Link>
              </div>
            </Col>
          </Row>
        </Col>
        <Col lg={7}>
          <Row>
            <Col lg={4}>
              <ul className="list-footer">
                <h5>COMPANY</h5>
                <li>
                  <Link href={'#'}>About Us</Link>
                </li>
                <li>
                  <Link href={'#'}>Our Leadership</Link>
                </li>
                <li>
                  <Link href={'#'}>Careers</Link>
                </li>
              </ul>
            </Col>
            <Col lg={4}>
              <ul className="list-footer">
                <h5>SERVICES</h5>
                <li>
                  <Link href={'#'}>About Us</Link>
                </li>
                <li>
                  <Link href={'#'}>Our Leadership</Link>
                </li>
                <li>
                  <Link href={'#'}>Careers</Link>
                </li>
              </ul>
            </Col>
            <Col lg={4}>
              <ul className="list-footer">
                <h5>USEFUL INFO</h5>
                <li>
                  <Link href={'#'}>Investor Relations</Link>
                </li>
                <li>
                  <Link href={'#'}>News</Link>
                </li>
                <li>
                  <Link href={'#'}>Sustainability</Link>
                </li>
                <li>
                  <Link href={'#'}>Contact Us</Link>
                </li>
                <li>
                  <Link href={'#'}>Security</Link>
                </li>
              </ul>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default function Footer() {
  return (
    <Container>
      <footer className="footer">
        <FooterTop />
        <FooterBotom />
      </footer>
    </Container>
  );
}
