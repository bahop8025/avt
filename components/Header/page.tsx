'use client';
import Image from 'next/image';
import Link from 'next/link';
import { Container } from 'react-bootstrap';
import imageLogo from '../../public/Logo_big.png';
import './header.scss';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faXmark } from '@fortawesome/free-solid-svg-icons';

function Header() {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  console.log(mobileMenuOpen);

  const toggleMobileMenu = () => {
    setMobileMenuOpen(!mobileMenuOpen);
  };

  return (
    <header className="header-container">
      <Container>
        <div className={`header ${mobileMenuOpen ? 'mobile-menu-open' : ''}`}>
          <div className="header_image">
            <Image src={imageLogo} alt="image" height={100} width={100} />
          </div>
          <div className={`header-menu ${mobileMenuOpen ? 'mobile-menu' : ''}`}>
            <ul>
              <li>
                <Link href={'#'}>Home</Link>
              </li>
              <li>
                <Link href={'#'}>About Us</Link>
              </li>
              <li>
                <Link href={'#'} className="menu-hover">
                  Products & Services
                </Link>
                {/* <div className="ccccccc">
                  <ul className="sub-menu">
                    <li>
                      <Link href={'#'}>Digital Entertainment</Link>
                    </li>
                    <li>
                      <Link href={'#'}>E-commerce</Link>
                    </li>
                    <li>
                      <Link href={'#'}>Digital Financial Services</Link>
                    </li>
                  </ul>
                </div> */}
              </li>
              <li>
                <Link href={'#'}>Investor Relations</Link>
              </li>
              <li>
                <Link href={'#'}>News</Link>
              </li>
              <li>
                <Link href={'#'}>Sustainability</Link>
              </li>
              <li>
                <Link href={'#'}>Careers</Link>
              </li>
              <li>
                <Link href={'#'}>Contact Us</Link>
              </li>
            </ul>
          </div>
          <div className="mobile-menu-toggle" onClick={toggleMobileMenu}>
            {mobileMenuOpen ? (
              <FontAwesomeIcon icon={faXmark} />
            ) : (
              <FontAwesomeIcon icon={faBars} />
            )}
          </div>
        </div>
      </Container>
    </header>
  );
}

export default Header;
