import React from 'react';
import './posts.scss';
import Link from 'next/link';
import Image from 'next/image';
import imageShoppe from '../../public/shopee.png';
import imagePosts from '../../public/img1.jpg';

export default function Posts() {
  return (
    <div className="post">
      <Link href={'#'} className="post-link">
        <div className="thumbnail">
          <Image src={imagePosts} alt="image" className="image-post" />
        </div>
        <p className="post-date">
          <span>Aug 29, 2023</span>
          <Image src={imageShoppe} alt="image" className="image-shop" />
        </p>
        <p className="post-title">
          Shopee launches book spotlighting local Malaysian sellers from every state
        </p>
        <p className="post-content">
          Shopee has introduced a compelling new book, titled &rsquo;Shopee
          Spotlights Local — Unveiling Malaysian Gems,&rsquo; encouraging Malaysians
          to embrace local e-commerce sellers to help preserve Malaysia&rsquo;s rich
          heritage and nurture homegrown talent.
        </p>
      </Link>
    </div>
  );
}
