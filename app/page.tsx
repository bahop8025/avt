import { OurMission } from '@/components/OurMission/page';
import RecentUpdates from '@/components/RecentUpdates/RecentUpdates';
import { Slider } from '@/components/Slider/page';
import { Container } from 'react-bootstrap';

export default function Home() {
  return (
    <>
      <Slider />
      <Container>
        <OurMission />
        <RecentUpdates />
      </Container>
    </>
  );
}
