import localFont from 'next/font/local';

export const openSans = localFont({
  src: './assets/fonts/Open_Sans/static/OpenSans-Light.ttf',
  variable: '--openSans-Light'
});
