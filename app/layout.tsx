import 'bootstrap/dist/css/bootstrap.min.css';
import type { Metadata } from 'next';
import Footer from '../components/Footer/page';
import Header from '../components/Header/page';
import { openSans } from './fonts';
import './globals.scss';

export const metadata: Metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app'
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={`${openSans.variable}`}>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  );
}
